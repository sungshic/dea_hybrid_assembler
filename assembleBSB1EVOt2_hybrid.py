#!/usr/bin/python
__author__ = 'spark'

import os
import subprocess
from genomeassemblypipeline.preprocessing import PreProcessor
from genomeassemblypipeline.variantdiscovery import VariantCaller
from genomeassemblypipeline.annotateseq import SeqAnnotator
from Bio import SeqIO
import sys

from find_evo_insert import InsertBlaster

# make a copy of the environment
env = dict(os.environ)
#env['CLASSPATH']
#subprocess.call(['java', '-jar', 'GATK/GenomeAnalysisTK.jar'])


print 'hello world'

refseq_file = 'genomeassemblypipeline/data/RefGenome/AL009126_EVOt2/AL009126_evot2.gb'
refseq_format = 'gb'
refseq_fasta = 'genomeassemblypipeline/data/RefGenome/AL009126_EVOt2/AL009126_evot2.fasta'
outdir_base = 'genomeassemblypipeline/data/gandalf/basespace/bsb1_evot2/assembly/'
conseq_file = outdir_base+'consensus.fasta'
in_vcf_file = outdir_base+'raw_variants.vcf'
out_embl_file = 'bsb1evot2_vs_AL009126evot2_annotated.embl'

pre_processor = PreProcessor('genomeassemblypipeline/','AL009126_EVOt2',
			     'genomeassemblypipeline/data/gandalf/basespace/bsb1_evot2/SP2_S6_L001_R1_001.fastq.gz',
			     'genomeassemblypipeline/data/gandalf/basespace/bsb1_evot2/SP2_S6_L001_R2_001.fastq.gz',
                             refseq_fasta,
                             outdir_base)


#variant_caller = VariantCaller('data/RefGenome/AL009126.3/sequence.fasta',
variant_caller = VariantCaller('genomeassemblypipeline/', refseq_fasta,
                               outdir_base+'addrg_reads.bam',
                               outdir_base)


print '######## pre-processing raw reads... ########'
# run the pre-processing pipeline
print 'quality-based trimming'
pre_processor.trimSeqReads()

print '###############assembly de novo...'
pre_processor.assembleDeNovo()

print 'find a contig with EVO_insert'
ins_blaster = InsertBlaster('_query.fasta', outdir_base+'denovo/spades/output/contigs.fasta')
ins_blaster.executeBlast()
ins_blaster.makeAnnotatedSeq('_psg1729_evot2_payload.embl', '_evo_insert_denovo.fasta', 'genomeassemblypipeline/data/gandalf/basespace/bsb1_evot2/assembly/')

print 'aligning PE reads to reference...'
pre_processor.alignPairedEndReads('aligned_reads.sam')


print 'sorting aligned reads...'
pre_processor.sortSAMIntoBAM(outdir_base+'aligned_reads.sam', 'sorted_reads.bam') # this command requires lots of memory, 4096MB used

print 'build bam index for sorted_reads.bam'
pre_processor.buildBamIndex(outdir_base+'sorted_reads.bam')

print 'removing unmapped reads...'
pre_processor.removeUnmappedReads(outdir_base+'sorted_reads.bam', outdir_base+'filtered_sample_aligned.bam', outdir_base+'unmapped_reads.bam')

print 'build bam index for filtered_sample_aligned.bam'
pre_processor.buildBamIndex(outdir_base+'filtered_sample_aligned.bam')


print 'sorting the filtered bam...'
pre_processor.sortBAMIntoBAM(outdir_base+'filtered_sample_aligned.bam', 'filtered_sample_aligned_sorted.bam')
print 'marking duplicate reads...'
pre_processor.markDuplicates(outdir_base+'filtered_sample_aligned_sorted.bam', 'dedup_reads.bam', 'metrics.txt')
print 'adding read group...'
pre_processor.addReadGroup(outdir_base+'dedup_reads.bam', 'addrg_reads.bam')
print 'building bam index from read groups...'
pre_processor.buildBamIndex(outdir_base+'addrg_reads.bam')
print 'building reference fasta index...'
pre_processor.buildRefFastaIndex('genomeassemblypipeline/data/RefGenome/AL009126_EVOt2/AL009126_evot2.fasta', 'genomeassemblypipeline/data/RefGenome/AL009126_EVOt2/AL009126_evot2.dict')




print 'calling variants...'
variant_caller.callVariants()


# use glimmer to find putative ORFs
#subprocess.call(['tigr-glimmer', 'long-orfs', '-n', '-t' '1.15', 'data/results/clc/consensus.fasta', 'data/results/clc/consensus.longorfs'])
#subprocess.call(['./g3-from-scratch.sh', 'data/results/clc/consensus.fasta', 'data/results/clc/consensus_g3'])
#subprocess.call(['./g3-from-scratch.sh', 'data/RefGenome/AL009126.fasta', 'data/results/clc/refseq_g3'])


print 'finding a consensus fasta...'
subprocess.call(['genomeassemblypipeline/getconsensusfasta3.sh', refseq_fasta, outdir_base+'addrg_reads.bam', outdir_base+'consensus.fasta', './genomeassemblypipeline'])
# fastq to fasta
# the fastq file need to be truncated using sed command 'sed -n '1,70262 p' addrg_reads_clc_cons.fq > consensus.fasta'
# the header needs to change from @... to >...

seqannotator = SeqAnnotator(refseq_file, conseq_file, outdir_base, ref_format=refseq_format)

in_seq = seqannotator.parseSeqRecordFile(conseq_file)
ref_seq = seqannotator.parseSeqRecordFile(refseq_file)
print 'transfering annotations...'
in_seq_records = seqannotator.transferAnnotations(in_seq, ref_seq)
in_seq_records = seqannotator.fillSeqGaps(in_seq_records, ref_seq)
in_seq_records = seqannotator.transferVCFAnnotations(in_seq_records, ref_seq, in_vcf_file)
in_seq_records = seqannotator.addHeaderAnnotations(in_seq_records)
print 'writing embl file...'

seqannotator.writeEMBLFile(in_seq_records, out_embl_file)

print 'hello world'
