__author__='spark'

from Bio.Blast.Applications import NcbiblastnCommandline
from Bio.Blast import NCBIXML
import itertools

class ContigBlaster():
    __outfile = None
    __outformat = None
    __blast_task = None
    __blast_cmd = None
    __blast_handle = None
    __blast_results = None
    __evalue_cutoff = None

    def __init__(self, queryfile=None, subjectfile=None, evalue_cutoff=0.8):
        try:
            self.__outfile = '_pb_cache_out.xml'
            self.__outformat = 5 # xml
            self.__blast_task = 'blastn-short'
            self.__blast_cmd = 'blastn'
            self.__evalue_cutoff = evalue_cutoff

            if queryfile and subjectfile:
                self.initBlastFiles(queryfile, subjectfile)

            print 'ContigBlaster initialized.'
        except Exception as inst:
            print type(inst)
            print inst.args
            print inst
            raise # re-raise Exception

    def initBlastFiles(self, queryfile, subjectfile, is_clear_memory=False):
        self.__blast_handle = NcbiblastnCommandline(cmd=self.__blast_cmd, out=self.__outfile, outfmt=self.__outformat, query=queryfile, \
                    evalue=self.__evalue_cutoff, subject=subjectfile, task=self.__blast_task)
        if is_clear_memory:
            self.__blast_results = None # clear past results

    def runBlast(self):
        try:
            if self.__blast_handle:
                stdout, stderr = self.__blast_handle() # run the blast on the command line as specified by initBlastFiles()
                self.loadResults()
                return True
            else:
                return False
        except Exception as inst:
            print type(inst)
            print inst.args
            print inst
            raise # re-raise Exception

    def loadResults(self):
        try:
            self.__blast_results = NCBIXML.parse(open(self.__outfile))
        except Exception as inst:
            print type(inst)
            print inst.args
            print inst
            raise # re-raise Exception

    def getBlastRecords(self):
        return self.__blast_results

    def isMultiAlignment(self, blast_record, count_max=1):
        #alignment_count = len(blast_record.alignments[0].hsps)
        alignments = list(itertools.chain(*[a.alignments for a in blast_record if len(a.alignments) > 0])) # flattened list of alignment records
        hsp_list = list(itertools.chain(*[a.hsps for a in alignments if len(a.hsps) > 0]))

        alignment_count = len(hsp_list)
        if alignment_count > count_max:
            return True, alignment_count
        else:
            return False, alignment_count


#stdout,stderr = blastn_cline()

#blast_r = NCBIXML.parse(open('./opuntia.xml'))
