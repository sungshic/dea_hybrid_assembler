__author__='spark'

from blast.contigblaster import ContigBlaster
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import IUPAC
from genomeassemblypipeline.annotateseq import SeqAnnotator

class InsertBlaster():
    def __init__(self, query_filepath, subject_filepath):
        self._query_file = query_filepath
        self._subject_file = subject_filepath
        self.cbhandle = ContigBlaster()
        self.cbhandle.initBlastFiles(self._query_file, self._subject_file)
       
    def sortFunc(self, a):
        max_len = 0
        for alignment in a.alignments:
            for hsps in alignment.hsps:
                if hsps.align_length > max_len:
                    max_len = hsps.align_length
        return max_len

    def executeBlast(self):
        self.cbhandle.runBlast()
        self.cbhandle.loadResults()

    #refseq_file = 'psg1729_evot2_payload.embl'
    #denovo_file = '_evo_insert_denovo.fasta'
    #data_outdir = 'ratt/results/dump'
    def makeAnnotatedSeq(self, refseq_file, denovo_file, data_outdir):
        brlist = list(self.cbhandle.getBlastRecords())
        brlist_sorted = sorted(brlist, key=self.sortFunc, reverse=True)

        # make a SeqRecord out of the de novo assembled evo_insert
        evo_insert_seq = brlist_sorted[0].alignments[0].hsps[0].sbjct
        evo_insert_seqrec = SeqRecord(Seq(evo_insert_seq, IUPAC.ambiguous_dna), id='evo_insert_denovo')
        outhandle = open('_evo_insert_denovo.fasta', 'w')
        SeqIO.write(evo_insert_seqrec, outhandle, 'fasta')
        outhandle.close()

        seqannotator = SeqAnnotator(refseq_file, denovo_file, data_outdir+'ratt/results/dump', ref_format='embl')
        seqannotator.executeRATT(ratt_home_str='./lib/ratt', final_copyto_path=data_outdir)


#cbhandle = ContigBlaster()
#cbhandle.initBlastFiles('_query.fasta', 'contigs.fasta')
#pbhandle.initBlastFiles('_query.fasta', 'assembly_graph.fastg')
#cbhandle.runBlast()
#cbhandle.loadResults()

